use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Worker {
  pub activity: String,
  pub current_job: Option<u32>,
  pub description: String,
  pub direct_messaging_queue_name: String,
  pub instance_id: String,
  pub label: String,
  pub queue_name: String,
  pub sdk_version: String,
  pub short_description: String,
  pub system_info: SystemInfo,
  pub version: String,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct SystemInfo {
  pub docker_container_id: String,
  pub number_of_processors: Option<u32>,
  pub total_memory: Option<u32>,
  pub total_swap: Option<u32>,
  pub used_memory: Option<u32>,
  pub used_swap: Option<u32>,
}
