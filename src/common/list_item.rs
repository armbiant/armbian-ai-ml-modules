use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
#[serde(rename = "list_item")]
pub struct ListItem {
  /// Value returned on selected item
  pub id: String,
  /// Displayed value on user interface
  pub label: String,
}
