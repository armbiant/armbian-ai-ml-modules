use super::{JobProgression, JobStatus, Parameter};
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Job {
  pub id: u32,
  pub step_id: u32,
  pub workflow_id: u32,
  pub inserted_at: NaiveDateTime,
  pub updated_at: NaiveDateTime,
  pub name: String,
  pub last_worker_instance_id: Option<String>,
  #[serde(default, rename = "params")]
  pub parameters: Vec<Parameter>,
  #[serde(default)]
  pub progressions: Vec<JobProgression>,
  #[serde(default)]
  pub status: Vec<JobStatus>,
}

impl Job {
  pub fn get_last_progression(&self) -> Option<JobProgression> {
    let mut progressions = self.progressions.clone();

    progressions.sort_by(|a, b| a.partial_cmp(b).unwrap());

    progressions.first().cloned()
  }
}
