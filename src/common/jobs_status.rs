use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
pub struct JobsStatus {
  pub completed: usize,
  pub errors: usize,
  pub processing: usize,
  pub queued: usize,
  pub stopped: usize,
  pub skipped: usize,
  pub total: usize,
}
