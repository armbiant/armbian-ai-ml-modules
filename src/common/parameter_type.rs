use super::{Filter, MediaSegment, Requirement, SelectInput};
use schemars::{
  schema::{InstanceType, Schema, SchemaObject, SingleOrVec},
  JsonSchema,
};
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
#[serde(tag = "type", rename_all = "snake_case", rename = "type")]
pub enum ParameterType {
  ArrayOfMediaSegments {
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    default: Vec<MediaSegment>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    value: Vec<MediaSegment>,
    #[serde(default)]
    required: bool,
  },
  ArrayOfStrings {
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    default: Vec<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    value: Vec<String>,
    #[serde(default)]
    required: bool,
  },
  ArrayOfTemplates {
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    default: Vec<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    value: Vec<String>,
    #[serde(default)]
    required: bool,
  },
  Boolean {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    default: Option<bool>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    value: Option<bool>,
    #[serde(default)]
    required: bool,
  },
  Extended {
    #[serde(default)]
    default: serde_json::Value,
    #[serde(default)]
    value: serde_json::Value,
    #[serde(default)]
    required: bool,
  },
  Filter {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    default: Option<Filter>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    value: Option<Filter>,
    #[serde(default)]
    required: bool,
  },
  Integer {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    default: Option<u32>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    value: Option<u32>,
    #[serde(default)]
    required: bool,
  },
  Number {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    default: Option<f64>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    value: Option<f64>,
    #[serde(default)]
    required: bool,
  },
  Requirements {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    default: Option<Requirement>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    value: Option<Requirement>,
    #[serde(default)]
    required: bool,
  },
  SelectInput {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    default: Option<SelectInput>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    value: Option<SelectInput>,
    #[serde(default)]
    required: bool,
  },
  String {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    default: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    value: Option<String>,
    #[serde(default)]
    required: bool,
  },
  Template {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    default: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    value: Option<String>,
    #[serde(default)]
    required: bool,
  },
}

impl ParameterType {
  pub fn get_bool(&self) -> Option<bool> {
    match self {
      ParameterType::Boolean { value, .. } => *value,
      _ => None,
    }
  }

  pub fn get_string_array(&self) -> Option<Vec<String>> {
    match self {
      ParameterType::ArrayOfStrings { value, .. } => Some(value.clone()),
      _ => None,
    }
  }

  pub fn get_integer(&self) -> Option<u32> {
    match self {
      ParameterType::Integer { value, .. } => *value,
      _ => None,
    }
  }

  pub fn get_number(&self) -> Option<f64> {
    match self {
      ParameterType::Number { value, .. } => *value,
      _ => None,
    }
  }

  pub fn get_requirement(&self) -> Option<Requirement> {
    match self {
      ParameterType::Requirements { value, .. } => value.clone(),
      _ => None,
    }
  }

  pub fn get_string(&self) -> Option<String> {
    match self {
      ParameterType::String { value, .. } | ParameterType::Template { value, .. } => value.clone(),
      _ => None,
    }
  }
}

fn get_metadata_default(schema: &Schema) -> Option<&Value> {
  if let Schema::Object(object) = &schema {
    object
      .metadata
      .as_ref()
      .and_then(|metadata| metadata.default.as_ref())
  } else {
    None
  }
}

fn get_string_default_value(schema: &Schema) -> Option<String> {
  get_metadata_default(schema).and_then(|default| {
    if let Value::String(value) = default {
      Some(value.to_string())
    } else {
      None
    }
  })
}

fn get_integer_default_value(schema: &Schema) -> Option<u32> {
  get_metadata_default(schema).and_then(|default| {
    if let Value::Number(value) = default {
      value.as_f64().map(|integer| integer as u32)
    } else {
      None
    }
  })
}

fn get_number_default_value(schema: &Schema) -> Option<f64> {
  get_metadata_default(schema).and_then(|default| {
    if let Value::Number(value) = default {
      value.as_f64()
    } else {
      None
    }
  })
}

fn get_boolean_default_value(schema: &Schema) -> Option<bool> {
  get_metadata_default(schema).and_then(|default| {
    if let Value::Bool(value) = default {
      Some(*value)
    } else {
      None
    }
  })
}

fn get_array_of_strings_default_value(schema: &Schema) -> Vec<String> {
  get_metadata_default(schema)
    .and_then(|default| {
      if let Value::Array(value) = default {
        Some(
          value
            .iter()
            .map(|single_value| single_value.as_str().unwrap().to_string())
            .collect::<Vec<String>>(),
        )
      } else {
        None
      }
    })
    .unwrap_or_default()
}

fn get_array_of_media_segments_default_value(schema: &Schema) -> Vec<MediaSegment> {
  get_metadata_default(schema)
    .and_then(|default| {
      if let Value::Array(value) = default {
        Some(
          value
            .iter()
            .map(|single_value| {
              let map = single_value.as_object().unwrap();

              fn get_value(map: &Map<String, Value>, key: &str) -> u64 {
                map
                  .get(key)
                  .and_then(|value| value.as_i64())
                  .unwrap_or_default() as u64
              }

              let start = get_value(map, "start");
              let end = get_value(map, "end");

              MediaSegment { start, end }
            })
            .collect::<Vec<MediaSegment>>(),
        )
      } else {
        None
      }
    })
    .unwrap_or_default()
}

fn get_default_array_string_values(object: &Map<String, Value>, key: &str) -> Vec<String> {
  object
    .get(key)
    .map(|value| {
      if let Value::Array(values) = value {
        values
          .iter()
          .filter_map(|path_value| {
            if let Value::String(inner_value) = path_value {
              Some(inner_value.clone())
            } else {
              None
            }
          })
          .collect()
      } else {
        vec![]
      }
    })
    .unwrap_or_default()
}

fn get_filter_default_value(schema: &Schema) -> Option<Filter> {
  get_metadata_default(schema).and_then(|default| {
    if let Value::Object(value) = default {
      let ends_with = get_default_array_string_values(value, "ends_with");
      Some(Filter { ends_with })
    } else {
      None
    }
  })
}

fn get_select_input_default_value(schema: &Schema) -> Option<SelectInput> {
  get_metadata_default(schema).and_then(|default| {
    if let Value::Object(value) = default {
      let ends_with = get_default_array_string_values(value, "ends_with");
      Some(SelectInput { ends_with })
    } else {
      None
    }
  })
}

fn get_requirement_default_value(schema: &Schema) -> Option<Requirement> {
  get_metadata_default(schema).and_then(|default| {
    if let Value::Object(value) = default {
      let paths = get_default_array_string_values(value, "paths");
      Some(Requirement { paths })
    } else {
      None
    }
  })
}

enum ArrayInstanceType {
  String,
  MediaSegments,
}

fn get_array_item_type(schema_object: &SchemaObject) -> Option<ArrayInstanceType> {
  if let Some(array) = &schema_object.array {
    if let Some(SingleOrVec::Single(item)) = &array.items {
      if let Schema::Object(schema_object) = item.as_ref() {
        if let Some(SingleOrVec::Single(instance_type)) = &schema_object.instance_type {
          if let InstanceType::String = **instance_type {
            return Some(ArrayInstanceType::String);
          }
        }
        if let Some("#/definitions/MediaSegment") = schema_object.reference.as_deref() {
          return Some(ArrayInstanceType::MediaSegments);
        }
      }
    }
  }
  None
}

#[derive(Debug)]
enum ObjectInstanceType {
  Filter,
  Requirements,
  SelectInput,
  Extended,
}

fn get_object_instance_type(schema_object: &SchemaObject) -> Option<ObjectInstanceType> {
  match schema_object.reference.as_deref() {
    Some("#/definitions/Requirement") => Some(ObjectInstanceType::Requirements),
    Some("#/definitions/Filter") => Some(ObjectInstanceType::Filter),
    Some("#/definitions/SelectInput") => Some(ObjectInstanceType::SelectInput),
    _ => Some(ObjectInstanceType::Extended),
  }
}

#[derive(Clone, Debug, PartialEq)]
enum GetParameterTypeError {
  Array,
  Object,
  Null,
}

fn get_parameter_type(
  schema: &Schema,
  object: &SchemaObject,
  instance_type: &InstanceType,
  required: bool,
) -> Result<ParameterType, GetParameterTypeError> {
  match *instance_type {
    InstanceType::Boolean => {
      let default = get_boolean_default_value(schema);
      Ok(ParameterType::Boolean {
        default,
        value: None,
        required,
      })
    }
    InstanceType::Number => {
      let default = get_number_default_value(schema);
      Ok(ParameterType::Number {
        default,
        value: None,
        required,
      })
    }
    InstanceType::String => {
      let default = get_string_default_value(schema);
      Ok(ParameterType::String {
        default,
        value: None,
        required,
      })
    }
    InstanceType::Integer => {
      let default = get_integer_default_value(schema);
      Ok(ParameterType::Integer {
        default,
        value: None,
        required,
      })
    }
    InstanceType::Array => {
      if let Some(schema_object) = get_array_item_type(object) {
        match schema_object {
          ArrayInstanceType::String => {
            let default = get_array_of_strings_default_value(schema);
            Ok(ParameterType::ArrayOfStrings {
              default,
              value: vec![],
              required,
            })
          }
          ArrayInstanceType::MediaSegments => {
            let default = get_array_of_media_segments_default_value(schema);
            Ok(ParameterType::ArrayOfMediaSegments {
              default,
              value: vec![],
              required,
            })
          }
        }
      } else {
        Err(GetParameterTypeError::Array)
      }
    }
    InstanceType::Object => match get_object_instance_type(object) {
      Some(ObjectInstanceType::Filter) => {
        let default = get_filter_default_value(schema);
        Ok(ParameterType::Filter {
          default,
          value: None,
          required,
        })
      }
      Some(ObjectInstanceType::Requirements) => {
        let default = get_requirement_default_value(schema);
        Ok(ParameterType::Requirements {
          default,
          value: None,
          required,
        })
      }
      Some(ObjectInstanceType::SelectInput) => {
        let default = get_select_input_default_value(schema);
        Ok(ParameterType::SelectInput {
          default,
          value: None,
          required,
        })
      }
      Some(ObjectInstanceType::Extended) => Ok(ParameterType::Extended {
        default: Value::Null,
        value: Value::Null,
        required,
      }),
      None => Err(GetParameterTypeError::Object),
    },
    InstanceType::Null => Err(GetParameterTypeError::Null),
  }
}

impl TryFrom<&Schema> for ParameterType {
  type Error = String;

  fn try_from(schema: &Schema) -> Result<ParameterType, Self::Error> {
    if let Schema::Object(object) = schema.clone() {
      match object.instance_type.clone() {
        None => match get_object_instance_type(&object) {
          Some(ObjectInstanceType::Filter) => Ok(ParameterType::Filter {
            default: None,
            value: None,
            required: true,
          }),
          Some(ObjectInstanceType::Requirements) => Ok(ParameterType::Requirements {
            default: None,
            value: None,
            required: true,
          }),
          Some(ObjectInstanceType::SelectInput) => Ok(ParameterType::SelectInput {
            default: None,
            value: None,
            required: true,
          }),
          Some(ObjectInstanceType::Extended) => Ok(ParameterType::Extended {
            default: Value::Null,
            value: Value::Null,
            required: true,
          }),
          None => Err("No parameter type found".to_string()),
        },
        Some(SingleOrVec::Single(instance_type)) => {
          get_parameter_type(schema, &object, &instance_type, true)
            .map_err(|e| format!("Parameter type not found : {:?}", e))
        }
        Some(SingleOrVec::Vec(multiple)) => {
          let parameter_types: Vec<Result<ParameterType, GetParameterTypeError>> = multiple
            .iter()
            .map(|instance_type| get_parameter_type(schema, &object, instance_type, false))
            .collect();

          if parameter_types.len() != 2 {
            return Err(format!(
              "Unsupported number of parameter types ({}, expected 2)",
              parameter_types.len()
            ));
          }

          if !parameter_types.contains(&Err(GetParameterTypeError::Null)) {
            return Err("Unsupported multiple types".to_string());
          }

          if let Some(Ok(parameter_type)) = parameter_types
            .iter()
            .find(|parameter_type| parameter_type.is_ok())
          {
            Ok(parameter_type.clone())
          } else {
            Err("Parameter type not found".to_string())
          }
        }
      }
    } else {
      Err("No match on instance type".to_string())
    }
  }
}

#[cfg(test)]
mod parameter_type_from_schema {
  use super::*;
  use schemars::schema::{
    ArrayValidation,
    InstanceType::{Array, Boolean, Integer, Number, String},
    Metadata,
    Schema::Object,
    SchemaObject, SubschemaValidation,
  };
  use serde_json::{json, Map, Value};

  #[test]
  fn test_string() {
    let schema = Object(SchemaObject {
      instance_type: Some(SingleOrVec::Single(Box::new(String))),
      ..Default::default()
    });
    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::String {
        default: None,
        value: None,
        required: true
      }
    )
  }

  #[test]
  fn test_string_with_default() {
    let schema = Object(SchemaObject {
      metadata: Some(Box::new(Metadata {
        default: Some(Value::String("pouet".to_string())),
        ..Default::default()
      })),
      instance_type: Some(SingleOrVec::Single(Box::new(String))),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::String {
        default: Some("pouet".to_string()),
        value: None,
        required: true
      }
    )
  }

  #[test]
  fn test_integer() {
    let schema = Object(SchemaObject {
      instance_type: Some(SingleOrVec::Single(Box::new(Integer))),
      ..Default::default()
    });
    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::Integer {
        default: None,
        value: None,
        required: true
      }
    )
  }

  #[test]
  fn test_integer_with_default() {
    let schema = Object(SchemaObject {
      metadata: Some(Box::new(Metadata {
        default: Some(json!(42.0)),
        ..Default::default()
      })),
      instance_type: Some(SingleOrVec::Single(Box::new(Integer))),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::Integer {
        default: Some(42),
        value: None,
        required: true
      }
    )
  }

  #[test]
  fn test_number() {
    let schema = Object(SchemaObject {
      instance_type: Some(SingleOrVec::Single(Box::new(Number))),
      ..Default::default()
    });
    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::Number {
        default: None,
        value: None,
        required: true
      }
    )
  }

  #[test]
  fn test_number_with_default() {
    let schema = Object(SchemaObject {
      metadata: Some(Box::new(Metadata {
        default: Some(json!(42.0)),
        ..Default::default()
      })),
      instance_type: Some(SingleOrVec::Single(Box::new(Integer))),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::Integer {
        default: Some(42),
        value: None,
        required: true
      }
    )
  }

  #[test]
  fn test_boolean() {
    let schema = Object(SchemaObject {
      instance_type: Some(SingleOrVec::Single(Box::new(Boolean))),
      ..Default::default()
    });
    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::Boolean {
        default: None,
        value: None,
        required: true
      }
    )
  }

  #[test]
  fn test_boolean_with_default() {
    let schema = Object(SchemaObject {
      metadata: Some(Box::new(Metadata {
        default: Some(Value::Bool(false)),
        ..Default::default()
      })),
      instance_type: Some(SingleOrVec::Single(Box::new(Boolean))),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::Boolean {
        default: Some(false),
        value: None,
        required: true
      }
    )
  }

  #[test]
  fn test_array_of_strings() {
    let schema = Object(SchemaObject {
      instance_type: Some(SingleOrVec::Single(Box::new(Array))),
      array: Some(Box::new(ArrayValidation {
        items: Some(SingleOrVec::Single(Box::new(Object(SchemaObject {
          instance_type: Some(SingleOrVec::Single(Box::new(String))),
          ..Default::default()
        })))),
        ..Default::default()
      })),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::ArrayOfStrings {
        default: vec![],
        value: vec![],
        required: true
      }
    );
  }

  #[test]
  fn test_array_of_strings_with_default() {
    let schema = Object(SchemaObject {
      metadata: Some(Box::new(Metadata {
        default: Some(Value::Array(vec![
          Value::String("value1".to_string()),
          Value::String("value2".to_string()),
        ])),
        ..Default::default()
      })),
      instance_type: Some(SingleOrVec::Single(Box::new(Array))),
      array: Some(Box::new(ArrayValidation {
        items: Some(SingleOrVec::Single(Box::new(Object(SchemaObject {
          instance_type: Some(SingleOrVec::Single(Box::new(String))),
          ..Default::default()
        })))),
        ..Default::default()
      })),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::ArrayOfStrings {
        default: vec!["value1".to_string(), "value2".to_string()],
        value: vec![],
        required: true
      }
    );
  }

  #[test]
  fn test_array_of_media_segments() {
    let schema = Object(SchemaObject {
      instance_type: Some(SingleOrVec::Single(Box::new(Array))),
      array: Some(Box::new(ArrayValidation {
        items: Some(SingleOrVec::Single(Box::new(Object(SchemaObject {
          reference: Some("#/definitions/MediaSegment".to_string()),
          ..Default::default()
        })))),
        ..Default::default()
      })),
      ..Default::default()
    });
    let parameter_type: ParameterType = (&schema).try_into().unwrap();
    assert_eq!(
      parameter_type,
      ParameterType::ArrayOfMediaSegments {
        default: vec![],
        value: vec![],
        required: true
      }
    );
  }

  #[test]
  fn test_array_of_media_segments_with_default() {
    let mut media_segment_1 = Map::new();
    media_segment_1.insert("start".to_string(), json!(42));
    media_segment_1.insert("end".to_string(), json!(66));

    let mut media_segment_2 = Map::new();
    media_segment_2.insert("start".to_string(), json!(123));
    media_segment_2.insert("end".to_string(), json!(321));

    let schema = Object(SchemaObject {
      metadata: Some(Box::new(Metadata {
        default: Some(Value::Array(vec![
          Value::Object(media_segment_1),
          Value::Object(media_segment_2),
        ])),
        ..Default::default()
      })),
      instance_type: Some(SingleOrVec::Single(Box::new(Array))),
      array: Some(Box::new(ArrayValidation {
        items: Some(SingleOrVec::Single(Box::new(Object(SchemaObject {
          reference: Some("#/definitions/MediaSegment".to_string()),
          ..Default::default()
        })))),
        ..Default::default()
      })),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::ArrayOfMediaSegments {
        default: vec![
          MediaSegment { start: 42, end: 66 },
          MediaSegment {
            start: 123,
            end: 321
          }
        ],
        value: vec![],
        required: true
      }
    );
  }

  #[test]
  fn test_select_input() {
    let schema = Object(SchemaObject {
      reference: Some("#/definitions/SelectInput".to_string()),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::SelectInput {
        default: None,
        value: None,
        required: true
      }
    );
  }

  #[test]
  fn test_filter() {
    let schema = Object(SchemaObject {
      reference: Some("#/definitions/Filter".to_string()),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::Filter {
        default: None,
        value: None,
        required: true
      }
    );
  }

  #[test]
  fn test_requirement() {
    let schema = Object(SchemaObject {
      reference: Some("#/definitions/Requirement".to_string()),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::Requirements {
        default: None,
        value: None,
        required: true
      }
    );
  }

  #[test]
  fn test_extended() {
    let schema = Object(SchemaObject {
      subschemas: Some(Box::new(SubschemaValidation {
        any_of: Some(vec![
          Schema::Object(SchemaObject {
            reference: Some("#/definitions/Secret".to_string()),
            ..Default::default()
          }),
          Schema::Object(SchemaObject {
            instance_type: Some(SingleOrVec::Single(Box::new(InstanceType::Null))),
            ..Default::default()
          }),
        ]),
        ..Default::default()
      })),
      ..Default::default()
    });

    let parameter_type: ParameterType = (&schema).try_into().unwrap();

    assert_eq!(
      parameter_type,
      ParameterType::Extended {
        default: Value::Null,
        value: Value::Null,
        required: true
      }
    );
  }
}
