use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct JobProgression {
  pub datetime: DateTime<Utc>,
  pub progression: u8,
}

impl PartialOrd for JobProgression {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.progression.cmp(&other.progression))
  }
}
