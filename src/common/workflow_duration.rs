use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
pub struct WorkflowDuration {
  #[serde(default, skip_deserializing)]
  job_id: Option<usize>,
  order_pending: usize,
  processing: usize,
  response_pending: usize,
  total: usize,
  workflow_id: usize,
}
