use schemars::schema::RootSchema;
use semver::Version;
use serde::{Deserialize, Serialize};

/// Structure that contains configuration for that worker
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct WorkerDefinition {
  #[serde(flatten)]
  pub description: WorkerDescription,
  pub parameters: RootSchema,
}

/// Structure that contains worker description
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct WorkerDescription {
  pub instance_id: String,
  pub queue_name: String,
  pub direct_messaging_queue_name: String,
  pub label: String,
  pub short_description: String,
  pub description: String,
  pub version: Version,
  pub sdk_version: Version,
}
